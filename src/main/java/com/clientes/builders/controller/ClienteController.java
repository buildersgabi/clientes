package com.clientes.builders.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.clientes.builders.dto.ClienteDTO;
import com.clientes.builders.form.AtalizacaoClienteForm;
import com.clientes.builders.form.ClienteForm;
import com.clientes.builders.model.Cliente;
import com.clientes.builders.repository.ClienteRepository;

@RestController
@RequestMapping("/clientes")
public class ClienteController {

	@Autowired
	private ClienteRepository clienteRepository;

	@GetMapping("/listar")
	@Cacheable(value = "listaDeClientes")
	public Page<ClienteDTO> listarClientes(@RequestParam(required = false) String nomeCliente,
			@PageableDefault(sort = "id", direction = Direction.ASC, page = 0, size = 3) Pageable paginacao) {

		Page<Cliente> clientes;
		
		if (nomeCliente == null) {
			clientes = clienteRepository.findAll(paginacao);
		} else {
			clientes = clienteRepository.findByNomeIgnoreCaseContaining(nomeCliente, paginacao);
		}
		
		return ClienteDTO.converter(clientes);
	}
	
	@GetMapping("/profissao")
	public Page<ClienteDTO> listarProfissao(@RequestParam(required = true) String nomeProfissao,
			@PageableDefault(sort = "profissao", direction = Direction.ASC, page = 0, size = 2) Pageable paginacao) {
		
		if (nomeProfissao == null) {
			Page<Cliente> clientes = clienteRepository.findAll(paginacao);
			return ClienteDTO.converter(clientes);
		} else {
			Page<Cliente> clientes = clienteRepository.findClientePorProfissao(nomeProfissao, paginacao);
			return ClienteDTO.converter(clientes);
		}
	}
	
	@PostMapping("/cadastrar")
	@Transactional
	@CacheEvict(value = "listaDeClientes", allEntries = true)
	public ResponseEntity<ClienteDTO> cadastrar(@RequestBody @Valid ClienteForm clienteForm, UriComponentsBuilder uriBuilder) {
		Cliente cliente = clienteForm.converter();
		clienteRepository.save(cliente);
		
		URI uri = uriBuilder.path("/clientes/cadastrar{id}").buildAndExpand(cliente.getId()).toUri();
		
		return ResponseEntity.created(uri).body(new ClienteDTO(cliente));	
	}
	
	@GetMapping("/detalhe/{id}")
	public ResponseEntity<ClienteDTO> detalhar(@PathVariable Long id) {
		Optional<Cliente> cliente = clienteRepository.findById(id);
		if (cliente.isPresent()) {
			return ResponseEntity.ok(new ClienteDTO(cliente.get()));
		}
		
		return ResponseEntity.notFound().build();
	}
	
	@PutMapping("/atualizar/{id}")
	@Transactional
	@CacheEvict(value = "listaDeClientes", allEntries = true)
	public ResponseEntity<ClienteDTO> atualizar(@PathVariable Long id, @RequestBody @Valid AtalizacaoClienteForm form) {
		
		Optional<Cliente> cliente = clienteRepository.findById(id);
		if (cliente.isPresent()) {
			cliente = form.atualizar(id, clienteRepository);
			return ResponseEntity.ok(new ClienteDTO(cliente.get()));
		}
		return ResponseEntity.notFound().build();
	}
	
	@DeleteMapping("/deletar/{id}")
	@Transactional
	@CacheEvict(value = "listaDeClientes", allEntries = true)
	public ResponseEntity<?> deletar(@PathVariable Long id) {
		Optional<Cliente> optional = clienteRepository.findById(id);
		if (optional.isPresent()) {
			clienteRepository.deleteById(id);
			return ResponseEntity.ok().build();
		}
		return ResponseEntity.notFound().build();
	}
}
