package com.clientes.builders.form;

import java.util.Optional;

import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

import com.clientes.builders.model.Cliente;
import com.clientes.builders.repository.ClienteRepository;
import com.sun.istack.NotNull;

public class AtalizacaoClienteForm {

	@NotEmpty @NotNull @Length(min = 2)
	private String profissao;

	public String getProfissao() {
		return profissao;
	}

	public void setProfissao(String profissao) {
		this.profissao = profissao;
	}

	public Optional<Cliente> atualizar(Long id, ClienteRepository clienteRepository) {
		Optional<Cliente> optional = clienteRepository.findById(id);
		
		if (optional.isPresent()) {
			Cliente cliente = optional.get();
			cliente.setProfissao(this.profissao);
		}
		
		return optional;
	}	
}
