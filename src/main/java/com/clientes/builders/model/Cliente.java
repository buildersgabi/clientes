package com.clientes.builders.model;

import java.time.LocalDate;
import java.time.Period;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class Cliente {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String nome;
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	private LocalDate dataNascimento;
	private Integer idade;
	private String profissao;
	
	public Cliente() {
	}

	public Cliente(String nome, LocalDate dataNascimento, String profissao) {
		this.nome = nome;
		this.dataNascimento = dataNascimento;
		this.idade = calculaIdade(dataNascimento);
		this.profissao = profissao;
	}
	
	private Integer calculaIdade(LocalDate dataNascimento) {
		LocalDate dataHoje = LocalDate.now();
		Integer idade = null;
		
		if (dataNascimento != null) {
			Period periodo = Period.between(dataNascimento, dataHoje);
			int dias = periodo.getDays();
			int meses = periodo.getMonths();
			int anos = periodo.getYears();
			idade =  anos;
		}
		System.out.println("------> Idade: " + idade);
		return idade;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public LocalDate getDataNascimento() {
		return dataNascimento;
	}
	public void setDataNascimento(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	public Integer getIdade() {
		return idade;
	}
	public void setIdade(Integer idade) {
		this.idade = idade;
	}

	public String getProfissao() {
		return profissao;
	}

	public void setProfissao(String profissao) {
		this.profissao = profissao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cliente other = (Cliente) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
