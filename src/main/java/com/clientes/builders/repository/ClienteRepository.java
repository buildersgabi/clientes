package com.clientes.builders.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.clientes.builders.model.Cliente;

public interface ClienteRepository extends JpaRepository<Cliente, Long>{

	List<Cliente> findByNome(String nomeCliente);

	@Query("SELECT c FROM Cliente c WHERE c.profissao = :profissao")
	Page<Cliente> findClientePorProfissao(@Param("profissao") String profissao, Pageable paginacao);

	Page<Cliente> findByNomeIgnoreCaseStartsWith(String nomeCliente, Pageable paginacao);
	
	@Query(name = "findByNomeCliente", value = "SELECT c FROM Cliente c WHERE c.nome LIKE %:nome%")
	Page<Cliente> findByNomeCliente(@Param("nome") String nome, Pageable paginacao);

	Page<Cliente> findByNomeIgnoreCaseContaining(String nomeCliente, Pageable paginacao);
}
