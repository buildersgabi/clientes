INSERT INTO CLIENTE(nome, data_nascimento, profissao) VALUES('Márcio Antonio dos Santos', '1930-01-01', 'Programador');
INSERT INTO CLIENTE(nome, data_nascimento, profissao) VALUES('Jacinto Leite Aquino Rego', '1968-04-08', 'Médico');
INSERT INTO CLIENTE(nome, data_nascimento, profissao) VALUES('Chevrolet da Silva Ford', '1959-09-30', 'Pedreiro');

INSERT INTO CLIENTE(nome, data_nascimento, profissao) VALUES('Naida Navinda Navolta Pereira', '1950-02-01', 'Diarista');
INSERT INTO CLIENTE(nome, data_nascimento, profissao) VALUES('Deusarina Venus de Milo', '1969-06-08', 'Arquiteta');
INSERT INTO CLIENTE(nome, data_nascimento, profissao) VALUES('Dolores Fuertes de Barriga', '1959-09-30', 'Secretária');

INSERT INTO CLIENTE(nome, data_nascimento, profissao) VALUES('Linda Blue Junia Sharon Mell Melina Marla Cyndi', '2005-10-06', 'Numerologista');
INSERT INTO CLIENTE(nome, data_nascimento, profissao) VALUES('Antonio Manso Pacífico de Oliveira Sossegado', '1999-12-31', 'Padeiro');
INSERT INTO CLIENTE(nome, data_nascimento, profissao) VALUES('Gohan Muniz Lopes', '2003-03-16', 'Musicólogo');

INSERT INTO CLIENTE(nome, data_nascimento, profissao) VALUES('Frankstefferson Jeffrey Jofre', '2010-12-31', 'Veterinário');
INSERT INTO CLIENTE(nome, data_nascimento, profissao) VALUES('Hericlapiton da Silva', '1980-01-25', 'Gerente de Projetos');
INSERT INTO CLIENTE(nome, data_nascimento, profissao) VALUES('Mangelstron Megatron', '2003-03-16', 'Mecânico');